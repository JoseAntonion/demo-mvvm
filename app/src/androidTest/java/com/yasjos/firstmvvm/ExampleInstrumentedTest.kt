package com.yasjos.firstmvvm

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.UiAutomatorScreenshotStrategy
import tools.fastlane.screengrab.locale.LocaleTestRule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(JUnit4::class)
class ExampleInstrumentedTest {

    @Rule
    @JvmField
    val localeTestRule = LocaleTestRule()

    @get:Rule
    var activityRule = ActivityTestRule(MainActivity::class.java, false, false)

    @Test
    fun mainActivityTest() {
        activityRule.launchActivity(null)
        Screengrab.setDefaultScreenshotStrategy(UiAutomatorScreenshotStrategy())

        val clearAllButton = Espresso.onView(ViewMatchers.withId(R.id.clear_all_or_delete_button))
        clearAllButton.perform(ViewActions.click())

        Screengrab.screenshot("captura1")

        addUser("jose", "jose@gmail.com")

        val saveButton = Espresso.onView(ViewMatchers.withId(R.id.save_or_update_button))
        saveButton.perform(ViewActions.click())

        Screengrab.screenshot("captura2")

        val itemSelected = Espresso.onView(ViewMatchers.withId(R.id.list_item_layout))
        itemSelected.perform(ViewActions.click())

        updateUser("jose123")

        saveButton.perform(ViewActions.click())

        Screengrab.screenshot("captura3")
    }

    private fun addUser(userName: String, email: String) {
        val nameEditText = Espresso.onView(ViewMatchers.withId(R.id.name_text))
        nameEditText.perform(ViewActions.replaceText(userName), ViewActions.closeSoftKeyboard())
        val emailEditText = Espresso.onView(ViewMatchers.withId(R.id.email_text))
        emailEditText.perform(ViewActions.replaceText(email), ViewActions.closeSoftKeyboard())
    }

    private fun updateUser(userName: String? = null, email: String? = null) {
        if (userName != null) {
            val nameEditText = Espresso.onView(ViewMatchers.withId(R.id.name_text))
            nameEditText.perform(ViewActions.replaceText(userName), ViewActions.closeSoftKeyboard())
        }
        if (email != null) {
            val emailEditText = Espresso.onView(ViewMatchers.withId(R.id.email_text))
            emailEditText.perform(ViewActions.replaceText(email), ViewActions.closeSoftKeyboard())
        }
    }

    //@Test
    //fun useAppContext() {
    //    // Context of the app under test.
    //    val appContext = InstrumentationRegistry.getInstrumentation().targetContext
    //    assertEquals("com.yasjos.demomvvm", appContext.packageName)
    //}
}