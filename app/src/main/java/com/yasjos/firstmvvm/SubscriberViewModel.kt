package com.yasjos.firstmvvm

import android.util.Patterns
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yasjos.firstmvvm.db.Subscriber
import com.yasjos.firstmvvm.db.SubscriberRepository
import kotlinx.coroutines.launch

class SubscriberViewModel(private val repository: SubscriberRepository) : ViewModel(), Observable {

    val subscribers = repository.subscribers
    private var isUpdateOrDelete = false
    private lateinit var subscriberToUpdateOrDelete: Subscriber

    @Bindable
    val inputName = MutableLiveData<String>()

    @Bindable
    val inputEmail = MutableLiveData<String>()

    @Bindable
    val saveOrUpdateButton = MutableLiveData<String>()

    @Bindable
    val clearAllOrDeleteButton = MutableLiveData<String>()

    private val statusMessage = MutableLiveData<Event<String>>()
    val message: LiveData<Event<String>> get() = statusMessage

    init {
        saveOrUpdateButton.value = "Save"
        clearAllOrDeleteButton.value = "Clear All"
    }

    fun saveOrUpdate() {
        if (inputName.value == null)
            statusMessage.value = Event("Please enter subs name")
        else if (inputEmail.value == null)
            statusMessage.value = Event("Please enter subs email")
        else if (!Patterns.EMAIL_ADDRESS.matcher(inputEmail.value!!).matches()) {
            statusMessage.value = Event("Please enter a valid email")
        } else {
            if (isUpdateOrDelete) {
                subscriberToUpdateOrDelete.name = inputName.value!!
                subscriberToUpdateOrDelete.email = inputEmail.value!!
                update(subscriberToUpdateOrDelete)
            } else {
                val name = inputName.value!!
                val email = inputEmail.value!!
                insert(Subscriber(0, name, email))
                inputName.value = null
                inputEmail.value = null
            }
        }
    }

    fun clearAllOrDelete() {
        if (isUpdateOrDelete)
            delete(subscriberToUpdateOrDelete)
        else
            clearAll()
    }

    fun insert(subscriber: Subscriber) =
        viewModelScope.launch {
            val newRowId = repository.insert(subscriber)
            if (newRowId > -1) {
                statusMessage.value = Event("Subscriber Inserted Successfully $newRowId")
            } else {
                statusMessage.value = Event("Error occurred on INSERT")
            }
        }

    fun update(subscriber: Subscriber) =
        viewModelScope.launch {
            val rowNum = repository.update(subscriber)
            if (rowNum > 0) {
                inputName.value = null
                inputEmail.value = null
                isUpdateOrDelete = false
                subscriberToUpdateOrDelete = subscriber
                saveOrUpdateButton.value = "Save"
                clearAllOrDeleteButton.value = "Clear All"
                statusMessage.value = Event("$rowNum row(s) Updated Successfully")
            } else {
                statusMessage.value = Event("Error occurred in UPDATE")
            }
        }

    fun delete(subscriber: Subscriber) =
        viewModelScope.launch {
            val rowNum = repository.delete(subscriber)
            if (rowNum > 0) {
                inputName.value = null
                inputEmail.value = null
                isUpdateOrDelete = false
                subscriberToUpdateOrDelete = subscriber
                saveOrUpdateButton.value = "Save"
                clearAllOrDeleteButton.value = "Clear All"
                statusMessage.value = Event("$rowNum row(s) Deleted Successfully")
            } else {
                statusMessage.value = Event("Error Occurred in DELETE")
            }

        }

    fun clearAll() =
        viewModelScope.launch {
            val rowNum = repository.deleteAll()
            if (rowNum > 0) {
                statusMessage.value = Event("$rowNum Subscribers deleted Successfully")
            } else {
                statusMessage.value = Event("Error occurred in clearAll")
            }
        }

    fun initUpdateAndDelete(subscriber: Subscriber) {
        inputName.value = subscriber.name
        inputEmail.value = subscriber.email
        isUpdateOrDelete = true
        subscriberToUpdateOrDelete = subscriber
        saveOrUpdateButton.value = "Update"
        clearAllOrDeleteButton.value = "Delete"
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }


}